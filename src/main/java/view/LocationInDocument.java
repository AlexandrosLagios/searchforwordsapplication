package view;

import input.FileToSearch;

public class LocationInDocument
{
    protected String locationType;

    public LocationInDocument(FileToSearch file)
    {

        if (file.getFileType().equals("pdf") || file.getFileType().equals("doc") || file.getFileType().equals("docx"))
        {
            this.locationType = "page(s)";
        }
        else if(file.getFileType().equals("txt"))
        {
            this.locationType = "line(s)";
        }
        else if(file.getFileType().equals("xls"))
        {
            this.locationType = "sheet(s)";
        }
    }
}
