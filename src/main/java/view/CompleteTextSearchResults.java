package view;

import input.FileToSearch;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CompleteTextSearchResults
{
    protected List<TextSearchResults> results;
    protected FileToSearch file;

    public CompleteTextSearchResults() {}

    /**
     * An object that provides the complete search view for the document
     * and the list of pieces of text we have provided.
     * @param file a file for which we will provide the text view.
     * @param results the view for the different pieces of text we are searching for.
     */
    public CompleteTextSearchResults(FileToSearch file, List<TextSearchResults> results)
    {
        this.results = results;
        this.file = file;
    }

    /**
     * Builds a string by appending the TextResult for every piece of text that was searched.
     * It appends different location types for different types of file.
     * @return A string with the complete results of a search for a list of texts in a document.
     */
    public String showSearchResultsForAFile()
    {
        StringBuilder searchResults = new StringBuilder();

        if(file.getFileType().equals("xls") || file.getFileType().equals("xlsx"))
        {
            return showExcelSearchResults(searchResults);
        }

        for(TextSearchResults result: results)
        {
            if(!result.wasfound())
            {
                searchResults.append("The text \"" + result.getText() + "\" was not found in the document \""
                        + file.getFileName() + "\". \n\n");
                continue;
            }

            List<Integer> locations = (ArrayList) result.getLocations();
            int numberOfLocations = locations.size();
            searchResults.append("The text \"" + result.getText() + "\" was found in the document \""
                    + file.getFileName() + "\", in the following " + file.getLocationType() + "(s): \n");
            searchResults.append(file.getShortLocationType() + " " + locations.get(0));

            for(int i = 1; i < numberOfLocations; i++)
            {
                searchResults.append(", " + file.getShortLocationType() + " " + locations.get(i));
            }
            searchResults.append("\n\n");
        }

        return searchResults.toString();
    }

    /**
     * A separate method for xls and xlsx files that returns a string with the complete results of the search.
     * @param searchResults A StringBuilder that was initialized in the method "showSearchResultsForAFile()"
     * @return A string with the results of the search in an xls or xlsx file.
     */
    public String showExcelSearchResults(StringBuilder searchResults)
    {
        for(TextSearchResults result: results)
        {
            if(!result.wasfound())
            {
                searchResults.append("The text \"" + result.getText() + "\" was not found in the document \""
                        + file.getFileName() + "\": \n");
                continue;
            }

            Map<String, List<Integer>> locations = (Map) result.getLocations();
            searchResults.append("The text \"" + result.getText() + "\" was found in the document \""
                    + file.getFileName() + "\", in the following sheet(s)" +
                    " and column(s): \n");

            for (Map.Entry<String, List<Integer>> sheet : locations.entrySet())
            {
                String sheetName = sheet.getKey();
                searchResults.append("In sheet \"" + sheetName + "\" in the following column(s): ");
                List<Integer> columns = sheet.getValue();

                searchResults.append("col. " + (columns.get(0)));
                for(int i = 1; i < columns.size(); i++)
                {
                    searchResults.append(", col. " + (columns.get(i)));
                }

                searchResults.append("\n");
            }

            searchResults.append("\n");
        }
        return searchResults.toString();
    }
}
