package input;

import finders.*;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.List;

public class FileToSearch
{
    private String filePath;
    private String fileName;
    private File file;
    private String fileType;
    private String locationType;
    private String locationType2;
    private String shortLocationType;
    private TextFinder textFinder;
    protected List<String> textList;

    /**
     * The file that we are going to search. Depending on the file extension, it passes different values for
     * the location type (page, line, etc.) and it creates different TextFinder objects from the appropriate
     * subclass of TextFinder.
     * @param filePath
     */
    public FileToSearch(String filePath)
    {
        this.filePath = filePath;
        this.file = new File(filePath);
        this.fileName = file.getName();
        this.fileType = FilenameUtils.getExtension(fileName);
        switch (fileType)
        {
            case "pdf":
                this.locationType = "page";
                this.shortLocationType = "p.";
                this.textFinder = new PDFTextFinder(this);
                break;
            case "doc":
            case "docx":
                this.locationType = "paragraph";
                this.shortLocationType = "paragraph";
                this.textFinder = new DOCTextFinder(this);
                break;
            case "txt":
                this.locationType = "line";
                this.shortLocationType = "line";
                this.textFinder = new TXTTextFinder(this);
                break;
            case "xls":
                this.locationType = "column";
                this.locationType2 = "sheet";
                this.shortLocationType = "col.";
                break;
            case "xlsx":
                this.locationType = "column";
                this.locationType2 = "sheet";
                this.shortLocationType = "col.";
                this.textFinder = new XLSTextFinder(this);
                break;
            default:
                this.textFinder = null;
                break;
        }
    }

    public String getFilePath()
    {
        return filePath;
    }

    public File getFile()
    {
        return file;
    }

    public String getFileName()
    {
        return fileName;
    }

    public String getFileType()
    {
        return fileType;
    }

    public String getLocationType()
    {
        return locationType;
    }

    public String getShortLocationType()
    {
        return shortLocationType;
    }

    public String getLocationType2()
    {
        return locationType2;
    }

    public List<String> getTextList()
    {
        return textList;
    }

    public TextFinder getTextFinder()
    {
        return textFinder;
    }
}
