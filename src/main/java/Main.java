import finders.TextFinder;
import input.FileOrDirectoryChooser;
import input.FileToSearch;
import input.TextToSearch;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.xmlbeans.XmlException;
import view.CompleteTextSearchResults;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main
{
    public static List<FileToSearch> searchDirectory()
    {
        FileOrDirectoryChooser fileOrDirectoryChooser = new FileOrDirectoryChooser();
        List<File> filesInDirectory = fileOrDirectoryChooser.getAllTheFiles();
        List<FileToSearch> filesToSearch = new ArrayList<>();
        for(File fileInDirectory: filesInDirectory)
        {
            FileToSearch file = new FileToSearch(fileInDirectory.getPath());
            filesToSearch.add(file);
        }

        return filesToSearch;
    }

    public static String searchAllTheFiles(List<FileToSearch> filesToSearch)
    {
        StringBuilder completeResults = new StringBuilder();
        List<String> textList;
        TextToSearch textToSearch = new TextToSearch();
        textList = textToSearch.getTextList();

        for(FileToSearch fileToSearch: filesToSearch)
        {
            completeResults.append(searchFile(fileToSearch, textList));
            completeResults.append("\n\n");
        }

        return completeResults.toString();
    }

    /**
     * First, a FileOrDirectoryChooser is created, so that the user can choose a file in a local directory.
     * Currently, the supported file types are: .txt, .doc, .docx, .pdf, .xls and.xlsx.
     * Then, the file that is chosen is passed to a FileToSearch and the correct TextFinder is determined,
     * if the file type is supported by a finder.
     * A TextToSearch object is created, so that the user can write the pieces of text that are to be searched.
     * Finally, the complete results of the search and the name of the file are passed to an array with 2 strings
     * and are returned.
     * @return An array of two strings whith name of the file that was searched and the
     * complete results of the search.
     */
    public static String searchFile(FileToSearch file, List<String> textList)
    {
        TextFinder textFinder = file.getTextFinder();
        String results = "";

        if(textFinder == null)
        {
//            JOptionPane.showMessageDialog(null, "This file cannot be searched", "Unreadable file type!", JOptionPane.ERROR_MESSAGE);
//            int tryAgain = JOptionPane.showConfirmDialog(null, "Do you want to choose another file?", "Try Again?",JOptionPane.YES_NO_OPTION);
            return "The file " + file.getFileName() + " could not be searched.";
        }

        textFinder.setTextList(textList);

        CompleteTextSearchResults completeTextSearchResults = null;

        try
        {
            completeTextSearchResults = new CompleteTextSearchResults(file, textFinder.findTextInDocument());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (OpenXML4JException e)
        {
            e.printStackTrace();
        }
        catch (XmlException e)
        {
            e.printStackTrace();
        }

        results = completeTextSearchResults.showSearchResultsForAFile();

        return results;
    }

    public static void printResults(String results)
    {
        JLabel label = new JLabel(results);
        label.setFont(new Font("Dialog", Font.BOLD,16));
        JScrollPane scrollPane = new JScrollPane(label);
        scrollPane.setPreferredSize(new Dimension(700,600));

        JTextArea textArea = new JTextArea(results);
        textArea.setFont(new Font("Arial", Font.BOLD,18));
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        textArea.setMargin(new Insets(5,5,5,5));

        scrollPane.getViewport().setView(textArea);
        //Object message = scrollPane;
        JOptionPane.showMessageDialog(null,scrollPane,
                "Search Results", JOptionPane.INFORMATION_MESSAGE);
    }

//    public static void logResults(String[] results)
//    {
//        Logger logger = Logger.getLogger(counter + "_" + results[0]);
//
//        logger.log(Level.INFO, results[1]);
//    }

    public static void main(String[] args)
    {
//        logResults(searchFile());
//        System.out.println(Main.class.getClassLoader().getResource("logging.properties"));
//        JOptionPane.showMessageDialog(null,searchFile()[1],
//                "Search Results", JOptionPane.INFORMATION_MESSAGE);
        printResults(searchAllTheFiles(searchDirectory()));
        System.exit(1);
    }
}
