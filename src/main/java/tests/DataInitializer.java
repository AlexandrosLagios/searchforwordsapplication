package tests;

import input.FileToSearch;

import java.util.ArrayList;
import java.util.List;

public class DataInitializer
{
    public static FileToSearch initializeData()
    {
        List<String> wordList = new ArrayList<>();
        wordList.add("mitosis");
        wordList.add("RNA");
        wordList.add("DNA");
        wordList.add("manipulation");
        wordList.add("terminal");
        wordList.add("eugenics");
        wordList.add("valley");
        wordList.add("node");

        //String filePath = "G:\\Michio\\Documents\\Courses\\COLLECTION\\General\\Ebooks.For.Dummies.Collection\\G\\Genetics For Dummies.pdf";
        String filePath = "G:\\Michio\\Documents\\Programming Documents\\DNA.xlsx";
        FileToSearch file = new FileToSearch(filePath);
        //String filePath = "G:\\Michio\\Downloads\\nagel_bat.pdf";
        return file;
    }
}
