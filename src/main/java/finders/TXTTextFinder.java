package finders;

import input.FileToSearch;
import org.junit.jupiter.api.Test;
import view.TextSearchResults;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TXTTextFinder extends TextFinder
{
    
    public TXTTextFinder(FileToSearch file)
    {
        super(file);
    }
    
    @Override
    public List<TextSearchResults> findTextInDocument() throws IOException
    {
        List<TextSearchResults> results = new ArrayList<>();
        
        for (String text: textList)
        {
            results.add(findTextInTXT(text));
        }
        
        return results;
    }
    
    private TextSearchResults findTextInTXT(String text) throws IOException
    {
        List<Integer> locationList = new ArrayList<>();
        
//        BufferedReader reader;
//        reader = new BufferedReader(new FileReader(file.getFile()));
//        String line = reader.readLine();
    
//        while (line != null)
//        {
//            lineNumber++;
//            if(line.toLowerCase().contains(text))
//            {
//                locationList.add(lineNumber);
//            }
//            line = reader.readLine();
//        }
//
//        reader.close();
        
        Stream<String> stream = Files.lines(Paths.get(file.getFilePath()));
        List<String> lines = stream.collect(Collectors.toList());
        lines.stream().filter(line -> !line.toLowerCase().contains(text))
                .forEach(line -> locationList.add(lines.indexOf(line)+1));
        
        
        return getTextSearchResults(text, locationList);
    }
}
