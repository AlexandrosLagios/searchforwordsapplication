package finders;

import input.FileToSearch;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import view.TextSearchResults;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XLSTextFinder extends TextFinder
{
    public XLSTextFinder(FileToSearch file)
    {
        super(file);
    }

    @Override
    public List<TextSearchResults> findTextInDocument() throws IOException
    {
        List<TextSearchResults> results = new ArrayList<>();

        FileInputStream theFile = new FileInputStream(new File(file.getFilePath()));
        XSSFWorkbook workbook = new XSSFWorkbook(theFile);
        List<XSSFSheet> sheets = new ArrayList<>();
        for(int i = 0; i < workbook.getNumberOfSheets(); i++)
        {
            XSSFSheet sheet = workbook.getSheetAt(i);
            sheets.add(sheet);
        }

        for (String text: textList)
        {
            results.add(findTextInXLS(text, sheets));
        }

        return results;
    }

    private TextSearchResults findTextInXLS (String text, List<XSSFSheet> sheets) throws FileNotFoundException
    {
        Map<String, List<Integer>> locMap = new HashMap<>();
        for(XSSFSheet sheet : sheets)
        {
            String sheetName = sheet.getSheetName();
            for(Row row: sheet)
            {
                for(Cell cell: row)
                {
                    if(cell.getStringCellValue().toLowerCase().contains(text))
                    {
                        int colNumber = cell.getColumnIndex() + 1;
                        if(locMap.get(sheetName) == null)
                        {
                            List<Integer> locationList = new ArrayList<>();
                            locationList.add(colNumber);
                            locMap.put(sheetName, locationList);
                        }

                        else
                        {
                            locMap.get(sheetName).add(colNumber);
                        }
                    }
                }
            }
        }

        return getExcelTextSearchResults(text, locMap);
    }

    public TextSearchResults getExcelTextSearchResults(String text, Map<String, List<Integer>> locMap)
    {
        TextSearchResults textSearchResults = new TextSearchResults(text);

        if (locMap.isEmpty())
        {
            textSearchResults.setWasFound(false);
        }
        else
        {
            textSearchResults.setWasFound(true);
        }

        textSearchResults.setLocations(locMap);

        return textSearchResults;
    }
}
