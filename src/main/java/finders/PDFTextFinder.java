package finders;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import input.FileToSearch;
import view.TextSearchResults;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class PDFTextFinder extends TextFinder
{
    public PDFTextFinder(FileToSearch file)
    {
        super(file);
    }

    @Override
    public List<TextSearchResults> findTextInDocument() throws IOException
    {
        List<TextSearchResults> results = new ArrayList<>();
        PDFParser pdfParser = new PDFParser(new RandomAccessFile(file.getFile(), "r"));
        pdfParser.parse();
        COSDocument cosDocument = pdfParser.getDocument();
        PDFTextStripper pdfTextStripper = new PDFTextStripper();
        PDDocument document = new PDDocument(cosDocument);

        if(document.isEncrypted())
        {
            return null;
        }

        for(String text: textList)
        {
            results.add(findTextInPDF(text, document, pdfTextStripper));
        }

        document.close();
        cosDocument.close();

        return results;
    }

    /**
     * It searches for a particular piece of text in a PDF document and
     * @param text The text we are searching for.
     * @param document The complete parsed document in the form of parsed text.
     * @param pdfTextStripper a PDFTextStripper object that we have instantiated and
     *                        which will help us navigate the text of the document.
     * @return A TextSearchResults object for the given text.
     * @throws IOException
     */
    private TextSearchResults findTextInPDF(String text, PDDocument document, PDFTextStripper pdfTextStripper) throws IOException
    {
        List<Integer> locationList = new ArrayList<>();


        for(int i = 1; i <= document.getNumberOfPages(); i++)
        {
            pdfTextStripper.setStartPage(i);
            pdfTextStripper.setEndPage(i);

            if(pdfTextStripper.getText(document).toLowerCase().contains(text))
            {
                locationList.add(i);
            }
        }

        return getTextSearchResults(text, locationList);
    }
}
