package finders;

import input.FileToSearch;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import view.TextSearchResults;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.xmlbeans.XmlException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DOCTextFinder extends TextFinder
{
    public DOCTextFinder(FileToSearch file)
    {
        super(file);
    }

    @Override
    public List<TextSearchResults> findTextInDocument() throws IOException
    {
        List<TextSearchResults> results = new ArrayList<>();

        FileInputStream theFile = new FileInputStream(new File(file.getFilePath()));
        XWPFDocument document = new XWPFDocument(theFile);
        List<XWPFParagraph> paragraphs = document.getParagraphs();

        for (String text: textList)
        {
            results.add(findTextInDOC(text, paragraphs));
        }

        return results;
    }

    private TextSearchResults findTextInDOC (String text, List<XWPFParagraph> paragraphs) throws FileNotFoundException
    {
        List<Integer> locationList = new ArrayList<>();

        for (int i = 0; i < paragraphs.size(); i++)
        {
            String paragraphText = paragraphs.get(i).getParagraphText();
            if(paragraphText.toLowerCase().contains(text))
            {
                locationList.add(i+1);
            }
        }

        return getTextSearchResults(text, locationList);
    }
}
